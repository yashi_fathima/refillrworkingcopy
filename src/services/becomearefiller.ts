import firebase from "firebase";

export class BecomearefillrService{

    inputDetails(token:string,name:string,businessname:string,
        contact:number,street:string,suburb:string,city:string,
        state:string,post:number){
        firebase.database().ref("/BecomeRefiller/"+token).set({
            
            name: name,
            businessname: businessname,
            contact: contact,
            street: street,
            city: city,
            suburb: suburb,
            state:state,
            post: post

        })
       
    }
    inputSuggestDetails(token:string,locationname:string,locationaddress:string){
            firebase.database().ref("/SuggestRefiller/"+token).set({
                locationname : locationname,
                locationaddress: locationaddress

            })
    }
    

}