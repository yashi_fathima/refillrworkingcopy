//we are using the services to perform actions like 
//sign in, sign up, forgot password.
//change password needs to be implemented.

import * as firebase from "firebase";

export class AuthService {
    authenticatedUser: any;
    userProfile:any;
    newUser: any;
    id:string;

    signup(email:string,password:string, firstname:string,lastname: string,contactno:string,street:string,suburb:string,city:string,state:string,post:string) {
        
        return firebase.auth().createUserWithEmailAndPassword(email,password).then((newUser)=> {
             firebase.auth().signInWithEmailAndPassword(email,password).then((authenticatedUser) => {
                this.id= this.getActiveUser().uid;
                firebase.database().ref('/Registration Details/' + this.id).set({
                    firstname: firstname,
                    lastname: lastname,
                    contactno:contactno,
                    email:email,
                    password:password,
                    street: street,
                    suburb:suburb,
                    city:city,
                    state:state,
                    post:post
                }
                )
             })
         })
        
       
        
    }
    signin(email:string,password:string){
        return firebase.auth().signInWithEmailAndPassword(email,password);}

    logout(){
        firebase.auth().signOut();
    }
    resetpassword(email:string){
        return firebase.auth().sendPasswordResetEmail(email)
        
    }
    
    getActiveUser(){
        return firebase.auth().currentUser;
    }
}
