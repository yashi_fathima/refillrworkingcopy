import * as firebase from "firebase";

//This service is to read and write data to the database.


export class DatabaseService{
    track:number;
    savings: number;
 mylocations : string;
 myprofile: string;
    //writing the locations to the database
    writeData(city: string,nodeId: number, type: string, desc:string, lati: number, longi: number){
        //var refer = firebase.database();
        firebase.database().ref('/locations/'+city+'/' + nodeId).set({
            type: type,
            latitude: lati,
            longitude: longi,
            desc: desc
        });
    }
    //reading the locations from the database
   readData(city: string, id: number){
        const readDataref : firebase.database.Reference = firebase.database().ref('/locations/'+city+'/'+id);
        readDataref.on('value', Snapshot =>{
            this.mylocations = Snapshot.val();
            console.log(this.mylocations);
        })
        return this.mylocations;
    }

    storescanData(ID:string,scannedData: string, type:string){
        console.log("In storescandata"+type);
        console.log(ID);
        firebase.database().ref('/ScanData/'+ ID+'/').set({
            scannedData: scannedData  
        });
       this.increamentrewardValue(ID,type);
    }     

    increamentrewardValue(ID:string,type:string){
        console.log(type +"in increment")
        firebase.database().ref('/Rewards/'+ID +'/'+type).transaction((UID)=>{
        return UID+1;})
        
    }
addValue(ID:string, type:string, bottle:number,cup:number){
    console.log(ID,type);
    
  firebase.database().ref('/Rewards/'+ID +'/').set({
      bottle : bottle,
      cup:  cup
  })
}

readProfile(ID: string){
    console.log(ID);
    const readDataref1 : firebase.database.Reference = firebase.database().ref('/Registration Details/'+ID+'/city');
    readDataref1.once('value',Snapshot=>{
    this.myprofile = Snapshot.val();
    console.log("dasdas"+this.myprofile+"no"+ Snapshot.val());
   

})
return this.myprofile;

}
}