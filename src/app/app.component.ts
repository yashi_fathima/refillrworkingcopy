

import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController } from 'ionic-angular';


import * as firebase from 'firebase';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { RegistrationPage } from '../pages/registration/registration';
import { MapPage } from '../pages/map/map';
import { MaintabPage } from '../pages/maintab/maintab';
import { AuthService } from '../services/auth';
import { Skip1Page } from '../pages/skip1/skip1';
import { PublicfountainPage } from '../pages/publicfountain/publicfountain';
import { CafemapPage } from '../pages/cafemap/cafemap';
import { WaterstationmapPage } from '../pages/waterstationmap/waterstationmap';
import { WaterandcofeemapPage } from '../pages/waterandcofeemap/waterandcofeemap';



@Component({
  templateUrl: 'app.html',
  
})
export class MyApp {

  isAuthenticated = false
 // skipPage1 = Skip1Page;
  mapPage = MapPage;
  maintabPage = MaintabPage;
  //homePage = HomePage;
  loginPage = LoginPage;
  tabsPage = TabsPage;
  skip1Page = Skip1Page;
  registrationPage = RegistrationPage;
  publicfountainPage= PublicfountainPage;
  cafemapPage= CafemapPage;
  waterstationmapPage = WaterstationmapPage;
  waterandcofeemapPage = WaterandcofeemapPage;
  

  @ViewChild('nav') nav: NavController;


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private authService: AuthService) {

      //database initialization... firebase (key,database Url..etc)
      firebase.initializeApp({
        apiKey: "AIzaSyAED-uHzJUhN30NGPuMlBhipKMrNvTLfJ0",
      authDomain: "refillr-3ba40.firebaseapp.com",
      databaseURL: "https://refillr-3ba40.firebaseio.com",
    projectId: "refillr-3ba40",
    storageBucket: "refillr-3ba40.appspot.com",
    messagingSenderId: "1018468783084"
      });    
    firebase.auth().onAuthStateChanged(User =>{
      if(User){
        this.isAuthenticated = true;
        this.nav.setRoot(this.skip1Page);
      }else{
        this.isAuthenticated = false;
        this.nav.setRoot(this.loginPage);
      }
      
    })

    platform.ready().then(() => {
     
      statusBar.styleDefault();
      splashScreen.hide();
    });
    
  }
  onLoad(page: any){
    this.nav.setRoot(page);
    this.menuCtrl.close();
    
  }
  loadwaterstationmapPage(){
    this.nav.push(this.waterstationmapPage);
    this.menuCtrl.close();
  }
  loadwaterandcofeemapPage(){
    this.nav.push(this.waterandcofeemapPage);
    this.menuCtrl.close();
  }
  loadcafemapPage(){
    this.nav.push(this.cafemapPage);
    this.menuCtrl.close();
  }
  loadpublicfountainPage(){
    this.nav.push(this.publicfountainPage);
    this.menuCtrl.close();
  }
  onLogout(){
    this.authService.logout();
    this.menuCtrl.close();

  }
}