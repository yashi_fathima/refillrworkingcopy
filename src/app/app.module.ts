//import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
//import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
//import { Push, PushObject, PushOptions } from '@ionic-native/';
import { MyApp } from './app.component';
import { CafemapPage } from '../pages/cafemap/cafemap';
import { WaterstationmapPage } from '../pages/waterstationmap/waterstationmap';
import { PublicfountainPage } from '../pages/publicfountain/publicfountain';
import { TabsPage } from '../pages/tabs/tabs';
import { MapPage } from '../pages/map/map';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { Skip1Page } from '../pages/skip1/skip1';
import { Geolocation } from '@ionic-native/geolocation';
import { ShopPage } from '../pages/shop/shop';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { MainmenuPage } from '../pages/mainmenu/mainmenu';
import { MaintabPage } from '../pages/maintab/maintab';
import { HowtousePage } from '../pages/howtouse/howtouse';
import { AboutPage } from '../pages/about/about';
import { FeedbackPage } from '../pages/feedback/feedback';
import { JoinPage } from '../pages/join/join';
import { LoginPage } from '../pages/login/login';
import { PolicyPage } from '../pages/policy/policy';
import { SupportPage } from '../pages/support/support';
import { OrganizationPage } from '../pages/organization/organization';
import { TermsPage } from '../pages/terms/terms';


import { HomePage } from '../pages/home/home';
import { RegistrationPage } from '../pages/registration/registration';
import { AuthService } from '../services/auth';
import { BecomerefillrPage } from '../pages/becomerefillr/becomerefillr';
//import { SuggestrefillrPage } from '../pages/suggestrefillr/suggestrefillr';
//import { from } from 'rxjs/observable/from';
import { FeedbackService } from '../services/feedback';
//import { AngularFireModule } from 'angularfire2';
//import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireDatabaseModule } from 'angularfire2/database';
//import  firebase from 'firebase';
import { DatabaseService } from '../services/database';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QrscanPage } from '../pages/qrscan/qrscan';
//import { Camera } from '@ionic-native/camera';
//import { QRScanner } from '@ionic-native/qr-scanner';

import { ProfilePage } from '../pages/profile/profile';
//import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TrackPage } from '../pages/track/track';
import { HelpPage } from '../pages/help/help';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { BecomearefillrService } from '../services/becomearefiller';
import { MysavingsPage } from '../pages/mysavings/mysavings';
import { BecomerefillrfrontPage } from '../pages/becomerefillrfront/becomerefillrfront';
import { LocationsdataProvider } from '../providers/locationsdata/locationsdata';
import { NotificationsPage } from '../pages/notifications/notifications';
import { FunstuffPage } from '../pages/funstuff/funstuff';
import { WaterandcofeemapPage } from '../pages/waterandcofeemap/waterandcofeemap';
//import { GoogleMapsCluster } from '../providers/googlemapscluster';

//This module is to load all the necessary imports, dependencies,
//declarations, libraries  etc.., the application might need

@NgModule({
  declarations: [
    MyApp,
    CafemapPage,
    WaterstationmapPage,
    PublicfountainPage,
    TabsPage,
    MapPage,
    Skip1Page,
    ShopPage,
    MyaccountPage,
    MainmenuPage,
    MaintabPage,
    HowtousePage,
    AboutPage,
    FeedbackPage,
    JoinPage,
    LoginPage,
    MyaccountPage,
    PolicyPage,
    SupportPage,
    OrganizationPage,
    TermsPage,
    HomePage,
    RegistrationPage,
    BecomerefillrPage,
    //SuggestrefillrPage,
    
    QrscanPage,
    WaterandcofeemapPage,
    BecomerefillrfrontPage,
    ProfilePage,
    TrackPage,
    HelpPage,
    ChangepasswordPage,
    MysavingsPage,
    NotificationsPage,
    FunstuffPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    //NgxQRCodeModule,
    AgmCoreModule.forRoot({
      apiKey:  'AIzaSyBcb7RhIBwXrI5UXA4JH-OHKcvmY3dGYug',
      libraries: ["places"]
      
    }),
    AgmJsMarkerClustererModule,
  //  AgmSnazzyInfoWindowModule,
    AngularFireDatabaseModule,
    
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CafemapPage,
    WaterstationmapPage,
    PublicfountainPage,
    TabsPage,
    MapPage,
    Skip1Page,
    ShopPage,
    MyaccountPage,
    MainmenuPage,
    MaintabPage,
    HowtousePage,
    AboutPage,
    FeedbackPage,
    JoinPage,
    LoginPage,
    MyaccountPage,
    PolicyPage,
    SupportPage,
    OrganizationPage,
    TermsPage,
    HomePage,
    RegistrationPage,
    BecomerefillrPage,
   // SuggestrefillrPage,
    BecomerefillrfrontPage,
    QrscanPage,
    WaterandcofeemapPage,
    ProfilePage,
    TrackPage,
    HelpPage,
    ChangepasswordPage,
    MysavingsPage,
    NotificationsPage,
    FunstuffPage 


  ],
  providers: [
    StatusBar,
    SplashScreen,
  //  Camera,
    //QRScanner,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    AuthService,
    FeedbackService,
    DatabaseService,
   BecomearefillrService,
  //PhonegapLocalNotification,
    LocalNotifications,
    SocialSharing,
    LocationsdataProvider 
    
  ]
})
export class AppModule {
}
