import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicfountainPage } from './publicfountain';

@NgModule({
  declarations: [
    PublicfountainPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicfountainPage),
  ],
})
export class PublicfountainPageModule {}
