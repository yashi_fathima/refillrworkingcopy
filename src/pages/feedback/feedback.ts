//This page is to write feedback by the users for the Refillr app.

import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { FeedbackService } from '../../services/feedback';
import 'rxjs/Rx';
import { NgForm } from '@angular/forms';
//import { ThankyouPage } from '../thankyou/thankyou';

@Injectable()
@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {

  
ID: string;

  constructor(public navCtrl: NavController, 
              private authService: AuthService,
              private feedbackService: FeedbackService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController
              ){
  }
  //input the feedback form from the user and pass it to auth service
  onFeedback(form: NgForm ){
    const loading =this.loadingCtrl.create({
      content: 'Please wait'
     });
     loading.present();
    this.ID= this.authService.getActiveUser().uid; // Authenticated user can only write feedback
   
         this.feedbackService.userFeedback(this.ID,form.value.comments);
       console.log(this.ID);
       loading.dismiss();
        const alert= this.alertCtrl.create({
          title: "Thank you so much! We'll be in touch soon",
          buttons: ['Ok'],
          cssClass: 'foo'
       });
       this.navCtrl.pop();
       alert.present();
       
     // this.navCtrl.push(this.thankyouPage);
    }
} 
