//Page for skip page 1
import { Component } from '@angular/core';
import { Slides} from 'ionic-angular';
import { ViewChild } from '@angular/core';
import {IonicPage , NavController } from "ionic-angular";
import { MapPage } from "../map/map";
import { MaintabPage } from "../maintab/maintab";
//import { Skip2Page } from '../skip2/skip2';
@IonicPage()
@Component({
  selector: 'page-skip1',
  templateUrl: 'skip1.html',
})
export class Skip1Page {
  @ViewChild(Slides) slides : Slides;
  mapPage = MapPage;
  maintabPage = MaintabPage;
 
  constructor(public navCtrl: NavController){}
  onClickSkip(){
    this.navCtrl.push(MaintabPage);
  }
  //reference to go to next page used in html file. (binding)
 // skip2Page = Skip2Page;
  
}
