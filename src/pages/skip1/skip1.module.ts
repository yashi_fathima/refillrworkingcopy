import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Skip1Page } from './skip1';

@NgModule({
  declarations: [
    Skip1Page,
  ],
  imports: [
    IonicPageModule.forChild(Skip1Page),
  ],
})
export class Skip1PageModule {}
