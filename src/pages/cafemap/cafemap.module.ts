import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CafemapPage } from './cafemap';

@NgModule({
  declarations: [
    CafemapPage,
  ],
  imports: [
    IonicPageModule.forChild(CafemapPage),
  ],
})
export class CafemapPageModule {}
