import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
//import { ToastController} from "ionic-angular";
import { NavController, AlertController } from "ionic-angular";
/* Here we are displaying coffee shops in the map.
* More code needs to be implemented.
* No hard coded values. Latitude and longitude will be retrieved 
*from database
*/
//import { DatabaseService } from '../../services/database';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

import { BecomearefillrService } from '../../services/becomearefiller';
import { AuthService } from '../../services/auth';
import { DatabaseService } from '../../services/database';
//import { LocalNotifications } from '@ionic-native/local-notifications';


@Component({
  selector: 'page-cafemap',
  templateUrl: 'cafemap.html',
})
export class CafemapPage {
  @ViewChild("search")
  public searchElementRef: ElementRef;
  suggestname: string;
  suggestaddress: string;
  ID:string;
  public mylocations ={};
  constructor( private geolocation: Geolocation, public navCtrl: NavController,
    //public toastCtrl: ToastController, //private databaseService: DatabaseService,
    private mapsAPILoader: MapsAPILoader, 
    private alertCtrl: AlertController,
    private authService: AuthService,
    private Refillr : BecomearefillrService,
    private ngZone: NgZone,
    public databaseService : DatabaseService) {
      this.mylocations[72] = this.databaseService.readData("Melbourne",63);
      this.mylocations[73] = this.databaseService.readData("Melbourne",64);
      this.mylocations[74] = this.databaseService.readData("Melbourne",65);
      this.mylocations[75] = this.databaseService.readData("Melbourne",66);
      this.mylocations[76] = this.databaseService.readData("Melbourne",67);
      this.mylocations[77] = this.databaseService.readData("Melbourne",68);
      this.mylocations[78] = this.databaseService.readData("Melbourne",69);
      this.mylocations[79] = this.databaseService.readData("Melbourne",70);
      this.mylocations[80] = this.databaseService.readData("Melbourne",71);
      this.mylocations[81] = this.databaseService.readData("Melbourne",72);
      this.mylocations[82] = this.databaseService.readData("Melbourne",73);
      
    }

      lat: number;
      lng: number
      public searchControl: FormControl;
      
      ionViewDidLoad(){
        this.searchControl = new FormControl();
        //to get current location of the user
        this.getLocation();
        this.mapsAPILoader.load().then(() => {
          //implementing Auto-complete search for google maps
          let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          type: "address"
          });
        
        autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if(place.geometry === undefined || place.geometry === null){
              return;
            }
    
            this.lat = place.geometry.location.lat();
            this.lng = place.geometry.location.lng();
            })
        })
    
      })
       
      }
  getLocation(){
    this.geolocation.getCurrentPosition().then((res) => {
    this.lat= res.coords.latitude;
    this.lng= res.coords.longitude;
    //let location='lat '+res.coords.latitude+' lang '+res.coords.longitude;
    
    }).catch((error) => {
    console.log('Error getting location', error);
    });
  }
  
  lat1: number =  -37.80561757;
  lng1: number = 144.9856512;
  desc1: string="Cambridge Park";

  lat2: number = -37.80287869;
  lng2: number =144.9937946;
  desc2: string="Eddy Court Reserve";

  lat3: number =  -37.79638449;
  lng3: number =145.0001739;
desc3: string="Dights Falls Park";

  lat4: number =  -37.77793547;
  lng4: number =144.9891856;
  desc4: string="Bundara Street Reserve";

  lat5: number =  -37.79306811;
  lng5: number =144.981279;
desc5: string="Triangle Park";

  lat6: number =  -37.78252579;
  lng6: number =144.9855736;
  desc6: string="Mark Street Linear Park"; 

  lat7: number = -37.7842081;
  lng7: number =145.0018763;
desc7: string="Knott Reserve"; 

  lat8: number = -37.78334439;
  lng8: number =144.9933366;
  desc8: string="Janet Millman Reserve";

  lat9: number =  -37.78276622;
  lng9: number =144.9912764;
desc9: string="Batson Reserve";

  lat10: number =  -37.78348263;
  lng10: number =144.983466;
  desc10: string="Piedmontes Corner";

  lat11: number =  -37.78851231;
  lng11: number =144.9940479;
desc11: string="Mayors Park";

  lat12: number = -37.81165611;
  lng12: number =145.0089483;
  desc12: string="Williams Reserve"; 

  lat13: number = -37.80357143;
  lng13: number =144.9929077;
desc13: string="St. Phillips Reserve"; 

  lat14: number = -37.80317709;
  lng14: number =144.9792408;
  desc14: string="Atherton Gardens";

  lat15: number = -37.82259321;
  lng15: number =144.9998679;
desc15: string="Peppercorn Park";

lat16:number = -37.7206671;
lng16:number = 145.0462253;
desc16 ="La Trobe university";

suggestRefillr(){
  const alert= this.alertCtrl.create({
    title: 'Cant find a Refillr location? Suggest one!',
   
    inputs: [
      
      {
        label:'LOCATION NAME',
        placeholder: 'Location Name',
        type : 'text'
      },
       
      {
        label:'LOCATION ADDRESS',
        placeholder: 'Location Address',
        type : 'text'
      }
      
],
buttons:[
  {
    text:"Cancel",
    handler: data=>{
      console.log('cancel clicked ->' + JSON.stringify(data));
    }
  },
  {
    text:"Submit",
    handler: data=>{
      console.log('submit clicked ->' + JSON.stringify(data[0]));
      this.suggestname = JSON.stringify(data[0]);
      this.suggestname= this.suggestname.replace(/['"]+/g, '');
      this.suggestaddress = JSON.stringify(data[1]);
      this.suggestaddress= this.suggestaddress.replace(/['"]+/g, '');

      this.ID = this.authService.getActiveUser().uid;

      this.Refillr.inputSuggestDetails(this.ID,this.suggestname,this.suggestaddress);
      

    }
  }
],

    cssClass :'foo'
   });
   alert.present();
}
}
