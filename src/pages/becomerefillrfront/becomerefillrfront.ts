import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BecomerefillrPage } from '../becomerefillr/becomerefillr';

/**
 * Generated class for the BecomerefillrfrontPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-becomerefillrfront',
  templateUrl: 'becomerefillrfront.html',
})
export class BecomerefillrfrontPage {
  becomearefillrPage = BecomerefillrPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BecomerefillrfrontPage');
  }
  Onclickbecome(){
    this.navCtrl.push(BecomerefillrPage);
  }
}

