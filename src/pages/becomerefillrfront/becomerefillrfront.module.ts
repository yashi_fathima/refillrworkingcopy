import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BecomerefillrfrontPage } from './becomerefillrfront';

@NgModule({
  declarations: [
    BecomerefillrfrontPage,
  ],
  imports: [
    IonicPageModule.forChild(BecomerefillrfrontPage),
  ],
})
export class BecomerefillrfrontPageModule {}
