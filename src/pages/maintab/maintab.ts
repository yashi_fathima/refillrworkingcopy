import { Component } from '@angular/core';
import { MainmenuPage } from '../mainmenu/mainmenu';
import { MapPage } from '../map/map';
import { ShopPage } from '../shop/shop';
import { MyaccountPage } from '../myaccount/myaccount';
import { MysavingsPage } from '../mysavings/mysavings';
/**
 *  This page is to display the Main tab at the bottom of the page 
 * Inline html tag is used.
 * And template for html is not used
 * 
 */
@Component({
  selector: 'page-maintab',
  template: ` <ion-tabs selectedIndex="1">
  <ion-tab [root] = "mainmenuPage" tabIcon="custom-chat"></ion-tab>
  <ion-tab [root] = "mapPage" tabIcon="custom1-chat1"></ion-tab>
  <ion-tab [root] = "shopPage" tabIcon="custom2-chat2"></ion-tab>
  <ion-tab [root] = "myaccountPage"  tabIcon="custom3-chat3"></ion-tab>
  <ion-tab [root] = "mysavingsPage" tabIcon="custom4-chat4"></ion-tab>
</ion-tabs>
`
})
export class MaintabPage {

  //references for the variables that are used in the html
  mainmenuPage = MainmenuPage;
  mapPage = MapPage;
  shopPage = ShopPage;
  myaccountPage = MyaccountPage;
  mysavingsPage = MysavingsPage;

}
