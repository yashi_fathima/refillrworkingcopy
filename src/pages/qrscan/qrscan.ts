//This page is to open the camera with user permissions and allow the user to scan the 
//QR code

import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DatabaseService } from '../../services/database';
import { AuthService } from '../../services/auth';


/**
 * Generated class for the QrscanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qrscan',
  templateUrl: 'qrscan.html',
})
export class QrscanPage {
  //friendAddress: string;
 type: string;
  qrData = null;
  scannedCode = null;
  ID: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    private alertCtrl: AlertController,
    private databaseService : DatabaseService,
    private authService : AuthService) {
  }

 scanCode(){
 
  this.barcodeScanner.scan().then(barcodeData => {
    this.scannedCode = barcodeData.text;
  })

  const alert = this.alertCtrl.create({
    title: "Scanned for Water bottle/Coffee cup?",
    inputs: [
      {
        type: 'radio',
        label: 'Water Bottle',
        value: 'bottle'
      },
      {
        type: 'radio',
        label: 'Coffee Cup',
        value: 'cup'
      }
    ],
    buttons:[
    {
      text:"Cancel",
      handler: data=>{
        console.log('cancel clicked ->' + JSON.stringify(data));
      }
    },
      {
        text: "Ok",
        handler: data=>{

          console.log('Ok clicked ->'+ JSON.stringify(data));
            this.type = JSON.stringify(data);
            this.type= this.type.replace(/['"]+/g, '');
            this.ID = this.authService.getActiveUser().uid;
            if(this.type === 'bottle'){
              this.databaseService.storescanData(this.ID,this.scannedCode,"bottle");
            }
            else{
              this.databaseService.storescanData(this.ID,this.scannedCode,"cup");
            }
            
            
            
            
            console.log(this.type);
        }

      }
    
    ]
  });
  alert.present();
}

 
}
