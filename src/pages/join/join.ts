//This page has two buttons for the user "Become Refillr" and "Suggest Refillr" to join Refillr app 

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { BecomerefillrPage } from '../becomerefillr/becomerefillr';
import { SuggestrefillrPage } from '../suggestrefillr/suggestrefillr';
import { BecomerefillrfrontPage } from '../becomerefillrfront/becomerefillrfront';

/**
 * Generated class for the JoinPage page.
 * More code(logic) needs to be implemented. 
 * 
 */

@IonicPage()
@Component({
  selector: 'page-join',
  templateUrl: 'join.html',
})
export class JoinPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  //reference to the pages are created
  becomeRefillrfront = BecomerefillrfrontPage;
  suggestRefillr = SuggestrefillrPage;

}
