import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MysavingsPage } from './mysavings';

@NgModule({
  declarations: [
    MysavingsPage,
  ],
  imports: [
    IonicPageModule.forChild(MysavingsPage),
  ],
})
export class MysavingsPageModule {}
