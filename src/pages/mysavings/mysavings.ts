import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../services/auth';
//import { DatabaseService } from '../../services/database'; 
import * as firebase from 'firebase';
/**
 * Generated class for the MysavingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mysavings',
  templateUrl: 'mysavings.html',
})
export class MysavingsPage {
  ID: string;
  savings: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    //private dbService: DatabaseService,
    private authService: AuthService) {
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MysavingsPage');
    this.ID = this.authService.getActiveUser().uid;
    firebase.database().ref('/Rewards/'+this.ID+'/bottle').once('value', Snapshot=>{
      this.savings= Snapshot.val() * 3;
      })
    }
    
   

}
