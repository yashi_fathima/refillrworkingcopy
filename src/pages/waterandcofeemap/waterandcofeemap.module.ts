import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WaterandcofeemapPage } from './waterandcofeemap';

@NgModule({
  declarations: [
    WaterandcofeemapPage,
  ],
  imports: [
    IonicPageModule.forChild(WaterandcofeemapPage),
  ],
})
export class WaterandcofeemapPageModule {}
