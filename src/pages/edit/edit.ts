import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { AuthService } from '../../services/auth';
import { NgForm } from '@angular/forms';

/**
 * Generated class for the EditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {
form: NgForm;
  constructor(public navCtrl: NavController, public navParams: NavParams,
) {
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad EditPage');
    this.form.value
  }

}
