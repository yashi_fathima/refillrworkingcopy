import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AuthService } from '../../services/auth';
//import * as firebase from 'firebase';
/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl:LoadingController,
    public alertCtrl:AlertController,
    private authService: AuthService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }

  changePassword(email:string){
    const loading =this.loadingCtrl.create({
      content: 'Please wait'
     });
     loading.present();
   
    this.authService.resetpassword(email).then(()=>{
        loading.dismiss();
        const alert= this.alertCtrl.create({
          title: 'Password reset link sent!',
          buttons: ['Ok']
       });
       alert.present();
    }).catch(error =>{
      loading.dismiss();
      //if any error
      const alert= this.alertCtrl.create({
        title: 'Password reset failed!',
        message: error.message,
        buttons: ['Ok']
     });
     alert.present();
 });
  }
 
}
