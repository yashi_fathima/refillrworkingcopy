/** This page lets user to enter the login email id and password
 * and allows user to use Refillr application to the fullest
 */

import { Component } from '@angular/core';
import * as firebase from 'firebase'
import {NgForm } from "@angular/forms";
import { LoadingController,AlertController } from 'ionic-angular';
import { AuthService } from "../../services/auth";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AuthService]
})
export class LoginPage 
{
 constructor( private authService: AuthService,
  private loadingCtrl: LoadingController,
  private alertCtrl: AlertController,
  ){

}
onSignin(form: NgForm){  
  //to create the loading sign... UI effect
   const loading =this.loadingCtrl.create({
    content: 'Signing you in...'
   });
   loading.present();
   //pass the user details to auth service for email id and password verification
 this.authService.signin(form.value.email,form.value.password)
     .then(data =>{
       //to get the loading symbol out of the screen
      loading.dismiss();
    })
    .catch(error =>{
      loading.dismiss();
      //if any error
      const alert= this.alertCtrl.create({
        title: 'Login failed!',
        message: error.message,
        buttons: ['Ok']
     });
     alert.present();
 });
 
 }
 //when user forgets password...
 onForgotPassword()
 {
   //alert box to enter the email ID
   const prompt = this.alertCtrl.create({
     title: 'Enter your Email',
     message: "A new password will be sent to your Email",
     inputs: [
       {
         name: 'recoveremail',
         placeholder: 'abc@example.com'
       },
     ],
     buttons: [
       {
         text: 'Cancel',
         handler: data=> {
           console.log("Cancelled");
         }
       },
       {
         text: 'Submit',
         handler: data=> {
           //preloader...
           const loading =this.loadingCtrl.create({
            content: 'Reset Password'
           });
           loading.present();
          console.log(data.recoveremail);
          this.authService.resetpassword(data.recoveremail).then(()=>{
            //add toast
            loading.dismiss();
              const alert = this.alertCtrl.create({
                title: 'Check your email',
                subTitle: 'Password reset successful',
                buttons: ['OK']
  
              });
              alert.present();
           
          }, error => {
            loading.dismiss();
            const alert = this.alertCtrl.create({
              title: 'Error reseting password',
              subTitle: error.message,
              buttons: ['OK']

            });
            alert.present();
          });
         }
       }
     ]
   });
   prompt.present();

 }
 loginwithfb()
 {
   let provider = new firebase.auth.FacebookAuthProvider();
   firebase.auth().signInWithRedirect(provider).then (()=>
   {
   firebase.auth().getRedirectResult().then((result)=>{

    alert(JSON.stringify(result));
   }).catch(function(error)
   {
     alert(JSON.stringify(error))
   });

   })




 }
}