// This page is where the user can register and login to the Refillr app
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegistrationPage } from '../registration/registration';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  }
  //reference to the pages (used in html file)
  loginPage = LoginPage;
  registrationPage = RegistrationPage;
}
