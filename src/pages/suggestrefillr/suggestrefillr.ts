import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NgModel } from '@angular/forms';
import { AuthService } from '../../services/auth';
//import { BecomearefillrService } from '../../services/becomearefiller';
//import { SuggestthankyouPage } from '../suggestthankyou/suggestthankyou';

/**
 * Generated class for the SuggestrefillrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-suggestrefillr',
  templateUrl: 'suggestrefillr.html',
})
export class SuggestrefillrPage {
ID: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private authService: AuthService, //private becomeRefillr: BecomearefillrService,
    private loadingCtrl : LoadingController, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuggestrefillrPage');
  }
  submitSuggestRefillr(form: NgModel)
  {
    const loading =this.loadingCtrl.create({
      content: 'Please wait'
     });
     loading.present();
    this.ID= this.authService.getActiveUser().uid;
    // this.becomeRefillr.inputSuggestDetails(this.ID,form.value.street,form.value.suburb,form.value.city,form.value.state,
      //form.value.post);
      loading.dismiss();
        const alert= this.alertCtrl.create({
          title: 'Thank you! We will contact you soon.',
          buttons: ['Ok']
       });
       alert.present();

       this.navCtrl.pop();
  }
}
