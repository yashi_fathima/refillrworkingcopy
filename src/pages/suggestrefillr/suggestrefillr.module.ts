import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuggestrefillrPage } from './suggestrefillr';

@NgModule({
  declarations: [
    SuggestrefillrPage,
  ],
  imports: [
    IonicPageModule.forChild(SuggestrefillrPage),
  ],
})
export class SuggestrefillrPageModule {}
