import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HowtousePage page.
 *
 *
 */

@IonicPage()
@Component({
  selector: 'page-howtouse',
  templateUrl: 'howtouse.html',
})
export class HowtousePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goBack(){
    this.navCtrl.pop();
  }

}
