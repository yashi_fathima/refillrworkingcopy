//import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QrscanPage } from '../qrscan/qrscan';
import { ProfilePage } from '../profile/profile';
import { TrackPage } from '../track/track';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AlertController } from 'ionic-angular';
import { NotificationsPage } from '../notifications/notifications';
//import { Cordova } from '@ionic-native/core';

/**
 * Generated class for the MyaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage {

  notificationsPage = NotificationsPage;
  qrscanPage = QrscanPage;
  profilePage= ProfilePage;
  trackPage = TrackPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  //   private phonegaplocalnotification: PhonegapLocalNotification,
    private alertCtrl: AlertController, //private platform: Platform,
     private localNotifications: LocalNotifications, private socialsharing: SocialSharing) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaccountPage');
  }
  scheduleNotifications(){
    this.localNotifications.requestPermission().then(
         (permission)=> {
         if(permission){
 this.localNotifications.schedule({
  
    title: 'Refillr',
    text: 'Do not forget to carry your water bottle! Go plastic free! Save Earth!',
    trigger: { at: new Date(new Date().getTime()+3*1000) },
    led: 'FF0000',
    sound: null
});

    }
    })
    }

          message:string = "Happy to be a Refillr, Join me! Go Plastic Free! Save Earth";
          url:string ="www.refillr.com.au"
     sharemyimpact(){
         this.socialsharing.share(this.message,this.url)
                   .then(()=>{
                   console.log("Message Shared")
                    }).catch(()=>{
                console.log("Not shared")
                });

          }

          //Code to report the faulty fountains
     reportFaulty(){
          const alert= this.alertCtrl.create({
            title: 'Report Faulty Fountains',
            message: 'Please email us at <a href="hello@refillr.com.au">hello@refillr.com.au</a> or reach us on <a href="03 9403 6700">03 9403 6700</a> and we will let your local council know.Cheers!',
            buttons: ['OK'],
            cssClass :'foo'
           });
           alert.present();
          }
}
