import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FunstuffPage } from './funstuff';

@NgModule({
  declarations: [
    FunstuffPage,
  ],
  imports: [
    IonicPageModule.forChild(FunstuffPage),
  ],
})
export class FunstuffPageModule {}
