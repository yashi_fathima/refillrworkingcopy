import { Component } from "@angular/core";
import { CafemapPage } from "../cafemap/cafemap";
import { WaterstationmapPage } from "../waterstationmap/waterstationmap";
import { PublicfountainPage } from "../publicfountain/publicfountain";

//this page will display the tab bar used to display the coffee stations, water stations,
//and ;ublic fountains
//Here, we are using inline html instead of seperate html page
@Component ({
    selector: 'page-tabs',
    template: `
       `
})
export class TabsPage {
    cafemapPage = CafemapPage;
    waterstationmapPage = WaterstationmapPage;
    publicfountainmapPage = PublicfountainPage;
}