import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarkerdetailsPage } from './markerdetails';

@NgModule({
  declarations: [
    MarkerdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MarkerdetailsPage),
  ],
})
export class MarkerdetailsPageModule {}
