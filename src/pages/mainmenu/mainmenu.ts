import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { HowtousePage } from '../howtouse/howtouse';
import { AboutPage } from '../about/about';
import { FeedbackPage } from '../feedback/feedback';
import { OrganizationPage } from '../organization/organization';
import { PolicyPage } from '../policy/policy';
import { TermsPage } from '../terms/terms';
import { JoinPage } from '../join/join';
import { HelpPage } from '../help/help';
import { BecomerefillrfrontPage } from '../becomerefillrfront/becomerefillrfront';
import { FunstuffPage } from '../funstuff/funstuff';
//import { BecomerefillrPage } from '../becomerefillr/becomerefillr';
@Component({
  selector: 'page-mainmenu',
  templateUrl: 'mainmenu.html',
})
export class MainmenuPage {
  
  constructor(public navCtrl: NavController) {

  }
  //references to the pages that are used in html 
  becomearefillerfrontPage = BecomerefillrfrontPage;
  howtousePage = HowtousePage;
  aboutPage = AboutPage;
  feedbackPage = FeedbackPage;
  organizationPage = OrganizationPage;
  policyPage = PolicyPage;
  termsPage = TermsPage;
  joinPage = JoinPage;
  helpPage = HelpPage;
  funstuffPage = FunstuffPage;
 
  onClickBecome(){
    this.navCtrl.push(this.becomearefillerfrontPage);
  }  
}
