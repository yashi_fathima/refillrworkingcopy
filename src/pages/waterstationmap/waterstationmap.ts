import { Geolocation } from '@ionic-native/geolocation';
import { Component } from '@angular/core';
//import { DatabaseService } from '../../services/database';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
//import { ToastController} from "ionic-angular";
import { NavController, AlertController } from "ionic-angular";
import { AuthService } from '../../services/auth';
import { BecomearefillrService } from '../../services/becomearefiller';
import * as firebase from 'firebase';
import { DatabaseService } from '../../services/database';



//need to get data from database. Code already implemented in maps page.
//just need to copy it in this for water stations
@Component({
  selector: 'page-waterstationmap',
  templateUrl: 'waterstationmap.html',
})
export class WaterstationmapPage {
  @ViewChild("search")
  public searchElementRef: ElementRef;
  suggestname: string;
  suggestaddress: string;
  ID:string;
  public mylocations = {};
  constructor( private geolocation: Geolocation, public navCtrl: NavController,
  //  public toastCtrl: ToastController, //private databaseService: DatabaseService,
    private mapsAPILoader: MapsAPILoader, private ngZone: NgZone,
    private authService: AuthService,
    private Refillr: BecomearefillrService,
    private alertCtrl: AlertController,
    public databaseService: DatabaseService) {
      this.mylocations[1] = this.databaseService.readData("Melbourne",52);
      this.mylocations[2] = this.databaseService.readData("Melbourne",53);
      this.mylocations[3] = this.databaseService.readData("Melbourne",54);
      this.mylocations[4] = this.databaseService.readData("Melbourne",55);
      this.mylocations[5] = this.databaseService.readData("Melbourne",56);
      this.mylocations[6] = this.databaseService.readData("Melbourne",57);
      this.mylocations[7] = this.databaseService.readData("Melbourne",58);
      this.mylocations[8] = this.databaseService.readData("Melbourne",59);
      this.mylocations[9] = this.databaseService.readData("Melbourne",60);
      this.mylocations[10] = this.databaseService.readData("Melbourne",61);
      this.mylocations[11] = this.databaseService.readData("Melbourne",62);
     
    }
    public searchControl: FormControl;
   latnew: number;
   longnew: number;
   
   ngOnInit(){
     
  var pointsArr = [];
  firebase.database().ref('locations/GlenEira/data').on('value', snapshot => {
      snapshot.forEach(point=>{
         pointsArr.push([point.val().latitude, point.val().longitude]);
    })
     console.log(pointsArr); // shows your points in array
  })
   }

   
ionViewDidLoad(){

    this.searchControl = new FormControl();
    //to get current location of the user
    this.getLocation();
    this.mapsAPILoader.load().then(() => {
      //implementing Auto-complete search for google maps
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      type: "address"
      });
    
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        if(place.geometry === undefined || place.geometry === null){
          return;
        }

        this.latnew = place.geometry.location.lat();
        this.longnew = place.geometry.location.lng();
        })
    })

  })
 
}
//display thhe current location of the user
  getLocation(){
    this.geolocation.getCurrentPosition().then((res) => {
    this.latnew = res.coords.latitude;
    this.longnew= res.coords.longitude;
    
    }).catch((error) => {
    console.log('Error getting location', error);
    });
  }
//static data references used in html for demonstration purposes
  

suggestRefillr(){
  const alert= this.alertCtrl.create({
    title: 'Cant find a Refillr location? Suggest one!',
   
    inputs: [
      
      {
        label:'LOCATION NAME',
        placeholder: 'Location Name',
        type : 'text'
      },
       
      {
        label:'LOCATION ADDRESS',
        placeholder: 'Location Address',
        type : 'text'
      }
      
],
buttons:[
  {
    text:"Cancel",
    handler: data=>{
      console.log('cancel clicked ->' + JSON.stringify(data));
    }
  },
  {
    text:"Submit",
    handler: data=>{
      console.log('submit clicked ->' + JSON.stringify(data[0]));
      this.suggestname = JSON.stringify(data[0]);
      this.suggestname= this.suggestname.replace(/['"]+/g, '');
      this.suggestaddress = JSON.stringify(data[1]);
      this.suggestaddress= this.suggestaddress.replace(/['"]+/g, '');

      this.ID = this.authService.getActiveUser().uid;

      this.Refillr.inputSuggestDetails(this.ID,this.suggestname,this.suggestaddress);
      

    }
  }
],

    cssClass :'foo'
   });
   alert.present();
}

}
