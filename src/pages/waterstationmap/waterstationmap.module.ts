import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WaterstationmapPage } from './waterstationmap';

@NgModule({
  declarations: [
    WaterstationmapPage,
  ],
  imports: [
    IonicPageModule.forChild(WaterstationmapPage),
  ],
})
export class WaterstationmapPageModule {}
