import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import * as firebase from 'firebase';
import { AuthService } from '../../services/auth';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { SuggestrefillrPageModule } from '../suggestrefillr/suggestrefillr.module';
//import { NgModel } from '@angular/forms';
//import { AngularFireDatabase } from 'angularfire2/database';
//import { DatabaseService } from '../../services/database';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

 // public myprofile={};
  ID:string;
  firstname:string;
  
  lastname:string;
  contactno:number;
  email:SuggestrefillrPageModule;
  street:string;
  suburb:string;
  city:string;
  state:string;

  post:string;
  changepasswordPage = ChangepasswordPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private authService:AuthService,
   public loadingCtrl: LoadingController,
   public alertCtrl: AlertController)
    {
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    
      this.ID = this.authService.getActiveUser().uid;
console.log(this.ID);
     // this.myprofile[0] = this.databaseservice.readProfile(this.ID);
       firebase.database().ref('/Registration Details/'+this.ID+'/firstname').
                        once('value',Snapshot=>{
                          this.firstname = Snapshot.val();
                        })
                        console.log(this.firstname);
      firebase.database().ref('/Registration Details/'+this.ID+'/lastname').
                        once('value',Snapshot=>{
                          this.lastname = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/contactno').
                        once('value',Snapshot=>{
                          this.contactno = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/street').
                        once('value',Snapshot=>{
                          this.street = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/suburb').
                        once('value',Snapshot=>{
                          this.suburb = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/city').
                        once('value',Snapshot=>{
                          this.city = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/state').
                        once('value',Snapshot=>{
                          this.state = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/post').
                        once('value',Snapshot=>{
                          this.post = Snapshot.val();
                        })
                        firebase.database().ref('/Registration Details/'+this.ID+'/email').
                        once('value',Snapshot=>{
                          this.email = Snapshot.val();
                        })
  }
  updateProfile(firstname:string,lastname:string,contactno:number,street:string,suburb:string,city:string,state:string,post:string): void{
    const loading =this.loadingCtrl.create({
      content: 'Updating profile..'
     });
     loading.present();
    console.log("inside update")
    const personRef: firebase.database.Reference = firebase.database().ref('/Registration Details/'+this.ID+'/');
  personRef.update({
    firstname: firstname,
    lastname: lastname,
    contactno: contactno,
    street:street,
    suburb:suburb,
    city:city,
    state:state,
    post:post
    
  })
  loading.dismiss();
  const alert= this.alertCtrl.create({
    title: 'Profile Updated',
    buttons: ['Ok']
  });
  alert.present();

  }

}
