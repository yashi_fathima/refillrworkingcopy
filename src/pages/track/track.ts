import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { Chart } from 'chart.js';
import { AuthService } from '../../services/auth';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as firebase from 'firebase';
import { DatabaseService } from '../../services/database';


@IonicPage()
@Component({
  selector: 'page-track',
  templateUrl: 'track.html',
})
export class TrackPage {

  ID: string;
  waternum : number;
  coffeenum: number;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private AuthService: AuthService,
     private socialsharing: SocialSharing,
     private dbservice: DatabaseService)
     {
  }
 
  ionViewDidLoad()
  {
    this.ID = this.AuthService.getActiveUser().uid;
    firebase.database().ref('/Rewards/'+this.ID+'/bottle/').once('value',Snapshot =>{
      this.waternum = Snapshot.val();})
    firebase.database().ref('/Rewards/'+this.ID+'/cup/').once('value',Snapshot =>{
      this.coffeenum = Snapshot.val();})
   
  }
 
 
  ngOnInit(){
    this.ionViewDidLoad();
     
  }
  

Addon(type:string){
  console.log(type);
  if(type === 'bottle'){
    this.waternum = this.waternum+1;
  }
  else{
    this.coffeenum = this.coffeenum+1;
  }
  
 this.dbservice.addValue(this.ID,type,this.waternum,this.coffeenum);
  //this.ngOnInit();
  
}

message:string = "Happy to make an impact.";
link:string=null;

sharemyimpact(){
  this.socialsharing.share(this.message,this.link)
  .then(()=>{
    console.log("Message Shared")
  }).catch(()=>{
  });
}
}


