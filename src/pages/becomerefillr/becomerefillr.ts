// This page is to enter the details of the coffee cafe's who are interested to be a part of Refillr app.

//More code needs to be implemented.
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth';
import { BecomearefillrService } from '../../services/becomearefiller';
//import { BecomethankyouPage } from '../becomethankyou/becomethankyou';
import { LoadingController, AlertController } from 'ionic-angular'

@IonicPage()
@Component({
  selector: 'page-becomerefillr',
  templateUrl: 'becomerefillr.html',
})
export class BecomerefillrPage {
  ID: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private authService:AuthService,
    private becomeRefillr: BecomearefillrService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BecomerefillrPage');
  }
  submitBecomeRefillr(form: NgForm){
    const loading =this.loadingCtrl.create({
      content: 'Please wait'
     });
     loading.present();
    this.ID= this.authService.getActiveUser().uid;

    this.becomeRefillr.inputDetails(this.ID, form.value.name,form.value.businessname,
      form.value.contactno,form.value.street,form.value.suburb,form.value.city,form.value.state,form.value.post)
      loading.dismiss();
        const alert= this.alertCtrl.create({
          title: 'Thank you! We will contact you soon.',
          buttons: ['Ok']
       });
       alert.present();
      this.navCtrl.pop();
  }
}
