import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BecomerefillrPage } from './becomerefillr';

@NgModule({
  declarations: [
    BecomerefillrPage,
  ],
  imports: [
    IonicPageModule.forChild(BecomerefillrPage),
  ],
})
export class BecomerefillrPageModule {}
