import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { LoadingController,AlertController } from "ionic-angular";
import { AuthService } from '../../services/auth';

/**
 * This ts file is used to perform authentication of input fields and
 * connects to database to store the details.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage {
  
  constructor(private authService: AuthService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController){
}
//signup function takes the form entered in the html takes as input
onSignUp(form: NgForm){
  const loading = this.loadingCtrl.create({//UI effect, spinner can be seen
  content: 'Signing you up...'});
  loading.present();
  //calls the authservice for validation(which inturn connects to firebase and stores the data)
  //check for existing user/email ID
  this.authService.signup(form.value.email,form.value.password,form.value.firstname,form.value.lastname,form.value.contactno,form.value.street,form.value.suburb,form.value.city,form.value.state,form.value.post)
.then(data => {
  loading.dismiss();

})//Error handling when signup fails
.catch(error =>{
  loading.dismiss();
  const alert= this.alertCtrl.create({
    title: 'Signup failed!',
    message: error.message,
    buttons: ['Ok']
    
  });
  alert.present();
});
  
}
} 
  


