//  This page is to display the map for users. It is displayed even if the user is not logged in.
//

import { Component } from '@angular/core';
import { TabsPage } from '../tabs/tabs';
import { NavController } from "ionic-angular";
import { Geolocation } from '@ionic-native/geolocation';
import { LoginPage } from '../login/login';
import { RegistrationPage } from '../registration/registration';
import { DatabaseService } from '../../services/database';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
  
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',  
})
export class MapPage {
  //references to the pages used in html
  tabsPage = TabsPage;
  loginPage = LoginPage;
  registrationPage = RegistrationPage;
  public mylocations = {};

  @ViewChild("search")
  public searchElementRef: ElementRef;
 
  constructor( private geolocation: Geolocation, public navCtrl: NavController,
    //public toastCtrl: ToastController,
     private databaseService: DatabaseService,
    private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
      //writing the map details to the database... code needs modification
      this.databaseService.writeData("Brisbane",1617,"waterfountain", "fountain",-27.4490972,153.0292193 );
      this.databaseService.writeData("Brisbane",34592,"tap combination","bubbler and tap", -27.4488617,153.0293348);
      this.databaseService.writeData("Brisbane",79343,"TAP COMBINATION","BUBBLER & TAP",-27.4481868,153.0302469);
      this.databaseService.writeData("Brisbane",38056,"BUBBLER/DRINKING FOUNTAIN","DIABLED ACCESS BUBBLER",-27.4487881,153.0362064);
      this.databaseService.writeData("Brisbane",82580,"BUBBLER/DRINKING FOUNTAIN","BUBBLER",-27.4664855,153.0264626);
      this.databaseService.writeData("Brisbane",34586,"BUBBLER/DRINKING FOUNTAIN","BOWL SHAPED DRINKING FOUNTAIN",-27.4663534,153.0266365);
      this.databaseService.writeData("Brisbane",70020,"TAP COMBINATION","BUBBLER & TAP NEAR CHESS PAVEMENT",-27.4657953,153.0236887);
      this.databaseService.writeData("Brisbane",70023,"TAP COMBINATION","BUBBLER & ANTI-VANDAL TAP NEAR JACOBS LA",-27.4658626,153.0243761);
      this.databaseService.writeData("Brisbane",83970,"TAP COMBINATION","BUBBLER & 2 TAPS",-27.4670254,153.0215589);
      this.databaseService.writeData("Brisbane",83677,"TAP COMBINATION","BUBBLER & 2 TAPS",-27.4671073,153.0222037);
      this.databaseService.writeData("Brisbane",133888,"BUBBLER/DRINKING FOUNTAIN","DIABLED ACCESS BUBBLER",-27.4754079,153.0286773);
      this.databaseService.writeData("Brisbane",37218,"BUBBLER/DRINKING FOUNTAIN","DRINKING FOUNTAIN",-27.4788978,153.0287439);
      this.databaseService.writeData("Brisbane",133115,"BUBBLER/DRINKING FOUNTAIN","DIABLED ACCESS BUBBLER",-27.4772831,153.0304334);
      this.databaseService.writeData("Melbourne",1,"Local Park","clarke street park",-37.80353741,145.0016425);
      this.databaseService.writeData("Melbourne",2,"Local Park","Clarke Street City", -37.81459527,145.0137567);
      this.databaseService.writeData("Melbourne",3,"Park","Clarke Street City", -37.81576246,145.0138857);
      this.databaseService.writeData("Melbourne",4,"Local Park","Urban Arts Square", -37.81713599,144.9904413);
      this.databaseService.writeData("Melbourne",5,"Local Park","Egan Place Park", -37.81434015,144.9938536);
      this.databaseService.writeData("Melbourne",6,"Local Park","Egan Place Park", -37.81435132,144.9939583);
      this.databaseService.writeData("Melbourne",7,"Local Park","Alphington Park", -37.78420152,145.0298045);
      this.databaseService.writeData("Melbourne",8,"Local Park","Alexander Street Park ", -37.79590389,144.9915662);
      this.databaseService.writeData("Melbourne",9,"Local Park","Flockhart Reserve", -37.80808477,145.0080447);
      this.databaseService.writeData("Melbourne",10,"Local Park","McNamara Reserve", -37.79714824,144.9891865);
      this.databaseService.writeData("Melbourne",11,"Local Park","Victoria Park", -37.79828155,144.9965938);
      this.databaseService.writeData("Melbourne",12,"Local Park","Studley Street Park", -37.80142471,144.9969334);
      this.databaseService.writeData("Melbourne",13,"Local Park","Smith Reserve", -37.7941099,144.9820669);
      this.databaseService.writeData("Melbourne",15,"Drinking Fountain","South East quadrant of Bailey Reserve", -37.918617856659125, 145.06202364203824);
      this.databaseService.writeData("Melbourne",16,"Drinking fountain","North West quadrant of Bentleigh Hodgson Reserve",-37.92018168225073, 145.0419588960612);
      this.databaseService.writeData("Melbourne",17,"Bookworm Drink Fountain","Jersey Parade outside Carnegie Library and Community Centre",-37.88732427510812, 145.05811379923117);
      this.databaseService.writeData("Melbourne",18,"Drinking fountain","North West quadrant of Caulfield Park",-37.86986792116847, 145.0255151768361);
      this.databaseService.writeData("Melbourne",19,"Drinking fountain", "South West quadrant of Caulfield Park", -37.872056179026146, 145.02756401540293);
      this.databaseService.writeData("Melbourne",20,"Drinking fountain","North West quadrant of Caulfield Park", -37.87046563198243, 145.02918502897884);
      this.databaseService.writeData("Melbourne",21,"Drinking fountain","South East quadrant of Caulfield Park", -37.87284878966704, 145.03425524253794);
      this.databaseService.writeData("Melbourne",22,"Drinking fountain ", "North East quadrant of Caulfield Park", -37.872128046073506, 145.03380317614238);
      this.databaseService.writeData("Melbourne",23,"Drink fountain ", "south of pavilion between car park and sportground", -37.9262938958724, 145.06795233064702);
      this.databaseService.writeData("Melbourne",24,"Drinking fountain", "North quadrant of Centre Road Rotunda", -37.91864503303267, 145.0384822900343);
      this.databaseService.writeData("Melbourne",25,"Drinking fountain", "East quadrant of Clapperton Street Park", -37.917579209348624, 145.0247796938571);
      this.databaseService.writeData("Melbourne",26,"Drinking fountain","North East quadrant of Colin Street Park", -37.92414413890207, 145.05256719279524);
      this.databaseService.writeData("Melbourne",27,"Drinking fountain","centre of Dega Avenue Park", -37.929523193331754, 145.05138354857596);
      this.databaseService.writeData("Melbourne",28,"Drinking fountain","North East quadrant of Duncan McKinnon Reserve", -37.903585683642675, 145.06457958814883);
      this.databaseService.writeData("Melbourne",29,"Drinking fountain","North west quadrant of athletics track adjacent to bbq", -37.90562396283098, 145.06364824549738);
      this.databaseService.writeData("Melbourne",30,"Drinking fountain","North west side of track adjacent to picnic tables", -37.90550495550239, 145.0637272277546);
      this.databaseService.writeData("Melbourne",31,"Local Park","Wangaratta St / Stewart St Reserve",-37.82444861,144.9921706);
      this.databaseService.writeData("Melbourne",32,"Local Park","Lennox Street", -37.81085972,144.995724);
      this.databaseService.writeData("Melbourne",33,"Local Park","Oxford Street Park", -37.80671908,144.9842887);
      this.databaseService.writeData("Melbourne",14,"Local Park","Peppercorn Park",-37.82259321,144.9998679);
      this.databaseService.writeData("Melbourne",34,"Local Park","Cairns Reserve", -37.82259321,144.9998679);
      this.databaseService.writeData("Melbourne",35,"Local Park","Alexander Reserve",-37.81949646,144.9984017);
      this.databaseService.writeData("Melbourne",36,"Local Park","Atherton Gardens", -37.80317709,144.9792408);
      this.databaseService.writeData("Melbourne",37,"Local Park","Condell Street Reserve", -37.80235975,144.9806348);
      this.databaseService.writeData("Melbourne",38,"Local Park","George Street Reserve", -37.80308284,144.9813263);
      this.databaseService.writeData("Melbourne",39,"Local Park","Cambridge Park", -37.80561757,144.9856512);
      this.databaseService.writeData("Melbourne",40,"Local Park","Eddy Court Reserve", -37.80287869,144.9937946);
      this.databaseService.writeData("Melbourne",41,"Local Park","Dights Falls Park", -37.79638449,145.0001739);
      this.databaseService.writeData("Melbourne",42,"Local Park","Bundara Street Reserve", -37.77793547,144.9891856);


      this.databaseService.writeData("Melbourne",43,"Local Park","Triangle Park", -37.79306811,144.981279);
      this.databaseService.writeData("Melbourne",44,"Local Park","Mark Street Linear Park", -37.78252579,144.9855736);
      this.databaseService.writeData("Melbourne",45,"Local Park","Knott Reserve", -37.7842081,145.0018763);

      this.databaseService.writeData("Melbourne",46,"Local Park","Janet Millman Reserve", -37.78334439,144.9933366);
      this.databaseService.writeData("Melbourne",47,"Local Park","Batson Reserve", -37.78276622,144.9912764);
      this.databaseService.writeData("Melbourne",48,"Local Park","Piedmontes Corner", -37.78348263,144.983466);
      this.databaseService.writeData("Melbourne",49,"Local Park","Mayors Park", -37.78851231,144.9940479);
      this.databaseService.writeData("Melbourne",50,"Local Park","Williams Reserve", -37.81165611,145.0089483);
      this.databaseService.writeData("Melbourne",51,"Local Park","St. Phillips Reserve", -37.80357143,144.9929077);


      this.databaseService.writeData("Melbourne",60,"Coffee only","Funkie Brewster", -37.736083,145.0607793);
      this.databaseService.writeData("Melbourne",61,"Coffee and Water","fresh as cafe", -37.7262108,145.0685999);
      this.databaseService.writeData("Melbourne",62,"Coffee only","crate speciality cafe", -37.74598,145.0492653);
      this.databaseService.writeData("Melbourne",63,"Coffee and Water","hunter lane", -37.7424485,145.0646027);
      this.databaseService.writeData("Melbourne",64,"Coffee only","Cafe Qtee", -37.717193,145.0067633);
      this.databaseService.writeData("Melbourne",65,"Coffee and Water","The Chairman Coffee", -37.752674,145.0489953);
      this.databaseService.writeData("Melbourne",66,"Coffee Only","silas coffee", -37.7526532,144.9811435);
      this.databaseService.writeData("Melbourne",67,"Coffee and Water","lucchini cafe", -37.7393115,145.0018898);
      this.databaseService.writeData("Melbourne",68,"Coffee Only","lip cafe", -37.759362,145.0419453);
      this.databaseService.writeData("Melbourne",69,"Coffee and Water","sycamore", -37.719168,144.9908463);
      this.databaseService.writeData("Melbourne",70,"Coffee Only","izzy cafetaria", -37.7556716,145.0626703);

      this.databaseService.writeData("Melbourne",71,"Coffee Only","all heart cafe", -37.6952359,145.0105828);
      this.databaseService.writeData("Melbourne",72,"Coffee and Water","commune coffee", -37.811713,144.9735163);
      this.databaseService.writeData("Melbourne",73,"Coffee Only","sir barkely hotel", -37.7561841,145.0650872);
      this.databaseService.writeData("Melbourne",74,"Coffee and Water","red deli", -37.7566875,145.0667733);
      this.databaseService.writeData("Melbourne",75,"Coffee Only","red bea coffee roaster", -37.747911,145.0023173);
      this.databaseService.writeData("Melbourne",76,"Coffee and Water","sartoria", -37.747943,145.0020803);
      this.databaseService.writeData("Melbourne",77,"Coffee Only","boundary espresso", -37.7482837,145.0018418);
      this.databaseService.writeData("Melbourne",78,"Coffee and Water","dragon fly", -37.747251,145.0004954);
      this.databaseService.writeData("Melbourne",79,"Coffee Only","pomona", -37.736976,144.9929064);
      this.databaseService.writeData("Melbourne",80,"Coffee and Water","eaglemont", -37.7636217,145.0521859);
      this.databaseService.writeData("Melbourne",81,"Coffee Only","elixir urban eatery",  -37.7292106,144.9897559);
      this.databaseService.writeData("Melbourne",82,"Coffee and Water","northern soul", -37.755096,144.9988219);
      this.databaseService.writeData("Melbourne",83,"Coffee Only","elizabeth cafe", -37.7292154,144.9820038);
      this.databaseService.writeData("Melbourne",84,"Coffee and Water","mr peebles", -37.771979,145.0434153);

      this.databaseService.writeData("Melbourne",85,"Coffee Only","muffin break", -37.6537667,145.0165086);
      this.databaseService.writeData("Melbourne",86,"Coffee and Water","coffee hit", -37.8436858,144.9426723);
      this.databaseService.writeData("Melbourne",87,"Coffee Only","reservoir crocs", -37.7180248,144.9779577);
      this.databaseService.writeData("Melbourne",88,"Coffee and Water","daily harvest", -37.7727239,145.0579383);
      this.databaseService.writeData("Melbourne",89,"Coffee Only","greensborough muffin break", -37.70291,145.1006306);
      this.databaseService.writeData("Melbourne",90,"Coffee and Water","jamaica greensborough", -37.7030627,145.1004706);
      this.databaseService.writeData("Melbourne",91,"Coffee Only","mario's coffee house", -37.7751576,145.0368737);
      this.databaseService.writeData("Melbourne",92,"Coffee and Water","lentil as anything", -37.7628774,144.9980219);
      this.databaseService.writeData("Melbourne",93,"Coffee Only","daily kitchen", -37.704424,145.1028464);
      this.databaseService.writeData("Melbourne",94,"Coffee and Water","sookie la la", -37.7628977,144.9976016);
      this.databaseService.writeData("Melbourne",95,"Coffee Only","northcote bakeshop", -37.7635073,144.9974083);





      //reading the location information from the database
      console.log(this.databaseService.readData("Brisbane",1617).length);  //to display in log for debugging purposes
      this.mylocations[0] = this.databaseService.readData("Brisbane",1617);
      console.log(this.mylocations[0]);
      this.mylocations[1] = this.databaseService.readData("Brisbane",34592);
      console.log(this.mylocations[1]);
      this.mylocations[2] = this.databaseService.readData("Brisbane",79343);
      console.log(this.mylocations[2]);
      this.mylocations[3] = this.databaseService.readData("Brisbane",38056);
      console.log(this.mylocations[3]);
      this.mylocations[4] = this.databaseService.readData("Brisbane",82580);
      console.log(this.mylocations[4]);
      this.mylocations[5] = this.databaseService.readData("Brisbane",34586);
      console.log(this.mylocations[5]);
      this.mylocations[6] = this.databaseService.readData("Brisbane",70020);
      console.log(this.mylocations[6]);
      this.mylocations[7] = this.databaseService.readData("Brisbane",70023);
      console.log(this.mylocations[7]);
      this.mylocations[8] = this.databaseService.readData("Brisbane",83970);
      console.log(this.mylocations[8]);
      this.mylocations[9] = this.databaseService.readData("Brisbane",83677);
      console.log(this.mylocations[9]);
      this.mylocations[10] = this.databaseService.readData("Brisbane",133888);
      console.log(this.mylocations[10]);
      this.mylocations[11] = this.databaseService.readData("Melbourne",1);
      console.log(this.mylocations[11]);
      this.mylocations[12] = this.databaseService.readData("Melbourne",2);
      console.log(this.mylocations[12]);
      this.mylocations[13] = this.databaseService.readData("Melbourne",3);
      console.log(this.mylocations[13]);
      this.mylocations[14] = this.databaseService.readData("Melbourne",5);
      console.log(this.mylocations[14]);
      this.mylocations[15] = this.databaseService.readData("Melbourne",6);
      console.log(this.mylocations[15]);
      this.mylocations[16] = this.databaseService.readData("Melbourne",7);
      console.log(this.mylocations[16]);
      this.mylocations[17] = this.databaseService.readData("Melbourne",8);
      console.log(this.mylocations[17]);
      this.mylocations[18] = this.databaseService.readData("Melbourne",9);
      console.log(this.mylocations[18]);
      this.mylocations[19] = this.databaseService.readData("Melbourne",10);
      console.log(this.mylocations[19]);
      this.mylocations[20] = this.databaseService.readData("Melbourne",11);
      console.log(this.mylocations[20]);
      this.mylocations[21] = this.databaseService.readData("Melbourne",12);
      console.log(this.mylocations[21]);
      this.mylocations[22] = this.databaseService.readData("Melbourne",13);
      console.log(this.mylocations[22]);
      this.mylocations[23] = this.databaseService.readData("Melbourne",14);
      console.log(this.mylocations[23]);
      this.mylocations[24] = this.databaseService.readData("Melbourne",15);
      this.mylocations[25] = this.databaseService.readData("Melbourne",16);
      this.mylocations[26] = this.databaseService.readData("Melbourne",17);
      this.mylocations[27] = this.databaseService.readData("Melbourne",18);
      this.mylocations[28] = this.databaseService.readData("Melbourne",19);
      this.mylocations[29] = this.databaseService.readData("Melbourne",20);
      this.mylocations[30] = this.databaseService.readData("Melbourne",21);
      this.mylocations[31] = this.databaseService.readData("Melbourne",22);
      this.mylocations[32] = this.databaseService.readData("Melbourne",23);
      this.mylocations[33] = this.databaseService.readData("Melbourne",24);
      this.mylocations[34] = this.databaseService.readData("Melbourne",25);
      this.mylocations[35] = this.databaseService.readData("Melbourne",26);
      this.mylocations[36] = this.databaseService.readData("Melbourne",27);
      this.mylocations[37] = this.databaseService.readData("Melbourne",28);
      this.mylocations[38] = this.databaseService.readData("Melbourne",29);
      this.mylocations[39] = this.databaseService.readData("Melbourne",30);
      this.mylocations[40] = this.databaseService.readData("Melbourne",31);
      this.mylocations[41] = this.databaseService.readData("Melbourne",32);
      this.mylocations[42] = this.databaseService.readData("Melbourne",33);
      this.mylocations[43] = this.databaseService.readData("Melbourne",34);
      this.mylocations[44] = this.databaseService.readData("Melbourne",35);
      this.mylocations[45] = this.databaseService.readData("Melbourne",36);
      this.mylocations[46] = this.databaseService.readData("Melbourne",37);
      this.mylocations[47] = this.databaseService.readData("Melbourne",38);
      this.mylocations[48] = this.databaseService.readData("Melbourne",39);
      this.mylocations[49] = this.databaseService.readData("Melbourne",40);
      this.mylocations[50] = this.databaseService.readData("Melbourne",41);
      this.mylocations[51] = this.databaseService.readData("Melbourne",42);
      this.mylocations[52] = this.databaseService.readData("Melbourne",43);
      this.mylocations[53] = this.databaseService.readData("Melbourne",44);
      this.mylocations[54] = this.databaseService.readData("Melbourne",45);
      this.mylocations[55] = this.databaseService.readData("Melbourne",46);
      this.mylocations[56] = this.databaseService.readData("Melbourne",47);
      this.mylocations[57] = this.databaseService.readData("Melbourne",48);
      this.mylocations[58] = this.databaseService.readData("Melbourne",49);
      this.mylocations[59] = this.databaseService.readData("Melbourne",50);
      this.mylocations[60] = this.databaseService.readData("Melbourne",51);
      this.mylocations[61] = this.databaseService.readData("Melbourne",52);
      this.mylocations[62] = this.databaseService.readData("Melbourne",53);
      this.mylocations[63] = this.databaseService.readData("Melbourne",54);
      this.mylocations[64] = this.databaseService.readData("Melbourne",55);
      this.mylocations[65] = this.databaseService.readData("Melbourne",56);
      this.mylocations[66] = this.databaseService.readData("Melbourne",57);
      this.mylocations[67] = this.databaseService.readData("Melbourne",58);
      this.mylocations[68] = this.databaseService.readData("Melbourne",59);
      this.mylocations[69] = this.databaseService.readData("Melbourne",60);
      this.mylocations[70] = this.databaseService.readData("Melbourne",61);
      this.mylocations[71] = this.databaseService.readData("Melbourne",62);
      this.mylocations[72] = this.databaseService.readData("Melbourne",63);
      this.mylocations[73] = this.databaseService.readData("Melbourne",64);
      this.mylocations[74] = this.databaseService.readData("Melbourne",65);
      this.mylocations[75] = this.databaseService.readData("Melbourne",66);
      this.mylocations[76] = this.databaseService.readData("Melbourne",67);
      this.mylocations[77] = this.databaseService.readData("Melbourne",68);
      this.mylocations[78] = this.databaseService.readData("Melbourne",69);
      this.mylocations[79] = this.databaseService.readData("Melbourne",70);
      this.mylocations[80] = this.databaseService.readData("Melbourne",71);
      this.mylocations[81] = this.databaseService.readData("Melbourne",72);
      this.mylocations[82] = this.databaseService.readData("Melbourne",73);
      this.mylocations[83] = this.databaseService.readData("Melbourne",74);
      this.mylocations[84] = this.databaseService.readData("Melbourne",75);
      this.mylocations[85] = this.databaseService.readData("Melbourne",76);
      this.mylocations[86] = this.databaseService.readData("Melbourne",77);
      this.mylocations[87] = this.databaseService.readData("Melbourne",78);
      this.mylocations[88] = this.databaseService.readData("Melbourne",79);
      this.mylocations[89] = this.databaseService.readData("Melbourne",80);
      this.mylocations[90] = this.databaseService.readData("Melbourne",81);
      this.mylocations[91] = this.databaseService.readData("Melbourne",82);
      this.mylocations[92] = this.databaseService.readData("Melbourne",83);
      this.mylocations[93] = this.databaseService.readData("Melbourne",84);
      this.mylocations[94] = this.databaseService.readData("Melbourne",85);
      this.mylocations[95] = this.databaseService.readData("Melbourne",86);
      this.mylocations[96] = this.databaseService.readData("Melbourne",87);
      this.mylocations[97] = this.databaseService.readData("Melbourne",88);
      this.mylocations[98] = this.databaseService.readData("Melbourne",89);
      this.mylocations[99] = this.databaseService.readData("Melbourne",90);
      this.mylocations[100] = this.databaseService.readData("Melbourne",91);
      this.mylocations[101] = this.databaseService.readData("Melbourne",92);
      this.mylocations[102] = this.databaseService.readData("Melbourne",93);
      this.mylocations[103] = this.databaseService.readData("Melbourne",94);
      this.mylocations[104] = this.databaseService.readData("Melbourne",95);
      


  }

    

  latnew: number;
  longnew: number;
  public searchControl: FormControl;
      
      
  ionViewDidLoad(){
    this.searchControl = new FormControl();
    //to get current location of the user
    this.getLocation();
    this.mapsAPILoader.load().then(() => {
      //implementing Auto-complete search for google maps
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      type: "address"
      });
    
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        if(place.geometry === undefined || place.geometry === null){
          return;
        }

        this.latnew = place.geometry.location.lat();
        this.longnew = place.geometry.location.lng();
        })
    })

  })
        
}
//display thhe current location of the user
  getLocation(){
    this.geolocation.getCurrentPosition().then((res) => {
    this.latnew = res.coords.latitude;
    this.longnew= res.coords.longitude;
    //let location='lat '+res.coords.latitude+' lang '+res.coords.longitude;
    
    }).catch((error) => {
    console.log('Error getting location', error);
    });
  }
  
}